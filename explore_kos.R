library(data.table)
library(stringr)

# explore Kegg

krh <- fread('data_article/KEGG/krh00001_sel.keg', sep = '\t', header = F)
ml <- fread('data_article/KEGG/mlu00001_sel.keg', sep = '\t', header = F)
staph <- fread('data_article/KEGG/ssp00001_sel.keg', sep = '\t', header = F)
cva <- fread('data_article/KEGG/cva00001_sel.keg', sep = '\t', header = F)

all.list <- list(krh=krh, ml=ml, staph=staph, cva=cva)
all.list <- lapply(all.list, function(x) {x[str_detect(V2, '^K')]})
all.list <- lapply(all.list, function(x) {x[, KO := str_extract(V2, '^K[0-9][0-9][0-9][0-9][0-9]')]})
ko.list <- lapply(all.list, function(x) {unique(x$KO)})

combs <- combinat::combn(names(ko.list), 2)
par(mfrow = c(3, 3))
apply(combs, 2, function(x) {
  gplots::venn(ko.list[c(x)])
})
gplots::venn(ko.list)


only.krh <- ko.list$krh[!(ko.list$krh %in% c(ko.list$ml, ko.list$staph))]
only.ml <- ko.list$ml[!(ko.list$ml %in% c(ko.list$krh, ko.list$staph))]
only.staph <- ko.list$staph[!(ko.list$staph %in% c(ko.list$krh, ko.list$ml))]
krh.or.ml <-  union(ko.list$krh, ko.list$ml)[!(union(ko.list$krh, ko.list$ml)
                                               %in% c(ko.list$staph))]
staph.and.krh.or.ml <- intersect(ko.list$staph,  union(ko.list$krh, ko.list$ml))



res <- data.table(taxa = c(only.krh, only.ml, only.staph, krh.or.ml, staph.and.krh.or.ml),
           tag = c(rep('only.krh', length(only.krh)),
                   rep('only.ml', length(only.ml)),
                   rep('only.staph', length(only.staph)),
                   rep('krh.or.ml', length(krh.or.ml)),
                   rep('staph.and.krh.or.ml', length(staph.and.krh.or.ml))))
cols <- data.table(color =  c('yellow', 'red', 'blue', 'orange', 'green'),
                   tag = c('only.krh', 'only.ml', 'only.staph', 'krh.or.ml', 'staph.and.krh.or.ml'))
res <- merge(res, cols, by = 'tag')

write.table(res[tag %in% c('only.staph', 'krh.or.ml', 'staph.and.krh.or.ml'), c('taxa', 'color'), with=F], 'out_V4/to_kegg.tsv', col.names = F, row.names = F, quote = F, sep = '\t')
